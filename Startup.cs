using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using NightscoutAlertBot.Clients;
using NightscoutAlertBot.Utilities;
using NightscoutAlertBot.Workers;

namespace NightscoutAlertBot
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "NightscoutAlertBot", Version = "v1" });
            });
            services.AddHttpClient();

            var config = new EnvironmentConfig
            {
                NightscoutApiToken = Environment.GetEnvironmentVariable("NS_API_TOKEN"),
                NightscoutApiUrl = Environment.GetEnvironmentVariable("NS_API_URL"),
                NightscoutDbUrl = Environment.GetEnvironmentVariable("NS_DB_URL"),
                NightscoutDbPassword = Environment.GetEnvironmentVariable("NS_DB_PASSWORD"),
                NightscoutDbUsername = Environment.GetEnvironmentVariable("NS_DB_USERNAME"),
                NightscoutDbPort = int.TryParse(Environment.GetEnvironmentVariable("NS_DB_PORT"), out var nsDbPort) ? nsDbPort : 27017,
                NightscoutDbData = Environment.GetEnvironmentVariable("NS_DB_DATA"),
                NightscoutDbAuth = Environment.GetEnvironmentVariable("NS_DB_AUTH"),
                NightscoutDataSource = Enum.TryParse<Enums.DataSource>(Environment.GetEnvironmentVariable("NS_DATASOURCE"), true, out var dataSource) ? dataSource : Enums.DataSource.API,

                TelegramDbUrl = Environment.GetEnvironmentVariable("TG_DB_URL"),
                TelegramDbPassword = Environment.GetEnvironmentVariable("TG_DB_PASSWORD"),
                TelegramDbUsername = Environment.GetEnvironmentVariable("TG_DB_USERNAME"),
                TelegramDbPort = int.TryParse(Environment.GetEnvironmentVariable("TG_DB_PORT"), out var tgDbPort) ? tgDbPort : 27017,
                TelegramDbData = Environment.GetEnvironmentVariable("TG_DB_DATA"),
                TelegramDbAuth = Environment.GetEnvironmentVariable("TG_DB_AUTH"),
                TelegramBotToken = Environment.GetEnvironmentVariable("TG_BOT_TOKEN"),

                HighAlertValue = int.TryParse(Environment.GetEnvironmentVariable("HIGH_ALERT_VALUE"), out var highAlertValue) ? highAlertValue : 0,
                LowAlertValue = int.TryParse(Environment.GetEnvironmentVariable("LOW_ALERT_VALUE"), out var lowAlertValue) ? lowAlertValue : 0,
                PollInterval = int.TryParse(Environment.GetEnvironmentVariable("POLL_INTERVAL"), out var pollInterval) ? pollInterval : 300
            };

            services.AddSingleton(config);

            if (config.NightscoutDataSource == Enums.DataSource.API)
            {
                services.AddSingleton<INightscoutClient, NightscoutApiClient>();
            }
            else
            {
                services.AddSingleton<INightscoutClient, NightscoutDbClient>();
            }

            services.AddSingleton<ITelegramDbClient, TelegramDbClient>();
            services.AddSingleton<ITelegram, Workers.Telegram>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NightscoutAlertBot v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var telegram = app.ApplicationServices.GetService<ITelegram>();
            // var task = telegram.Test();
            // task.Wait();
            telegram.Start().ConfigureAwait(false);
        }
    }
}
