using System.Threading.Tasks;
using MongoDB.Driver;
using NightscoutAlertBot.Models;
using NightscoutAlertBot.Utilities;

namespace NightscoutAlertBot.Clients
{
    public class NightscoutDbClient : INightscoutClient
    {
        private IMongoCollection<NightscoutDbEntry> entryCollection;

        public NightscoutDbClient(EnvironmentConfig config)
        {
            var mongoSettings = new MongoClientSettings
            {
                Server = new MongoServerAddress(config.NightscoutDbUrl, config.NightscoutDbPort),
                Credential = MongoCredential.CreateCredential(config.NightscoutDbAuth, config.NightscoutDbUsername,
                    config.NightscoutDbPassword)
            };

            var mongoClient = new MongoClient(mongoSettings);
            entryCollection = mongoClient.GetDatabase(config.NightscoutDbData).GetCollection<NightscoutDbEntry>("entries");
        }

        public async Task<NightscoutEntry> GetLastEntry()
        {
            var list = await entryCollection.Find(e => e.Type == "sgv").SortByDescending(e => e.Date).Limit(1).ToListAsync();
            return list[0];
        }
    }
}