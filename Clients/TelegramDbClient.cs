using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using NightscoutAlertBot.Models;
using NightscoutAlertBot.Utilities;

namespace NightscoutAlertBot.Clients
{
    public class TelegramDbClient : ITelegramDbClient
    {
        private readonly IMongoCollection<TelegramDbEntry> _entryCollection;

        public TelegramDbClient(EnvironmentConfig config)
        {
            var mongoSettings = new MongoClientSettings
            {
                Server = new MongoServerAddress(config.TelegramDbUrl, config.TelegramDbPort),
                Credential = MongoCredential.CreateCredential(config.TelegramDbAuth, config.TelegramDbUsername,
                    config.TelegramDbPassword)
            };

            var mongoClient = new MongoClient(mongoSettings);
            _entryCollection = mongoClient.GetDatabase(config.TelegramDbData).GetCollection<TelegramDbEntry>("entries");
        }

        public async Task<ICollection<TelegramDbEntry>> GetEntries()
        {
            return await _entryCollection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<TelegramDbEntry> GetEntry(string chatId)
        {
            return (await _entryCollection.Find(e => e.ChatId == chatId).Limit(1).ToListAsync())[0];
        }

        public async Task<DeleteResult> RemoveEntry(TelegramDbEntry entry)
        {
            var existingEntry = (await _entryCollection.Find(e => e.ChatId == entry.ChatId).Limit(1).ToListAsync())[0];

            var filter = Builders<TelegramDbEntry>.Filter.And(
                    Builders<TelegramDbEntry>.Filter.Eq(x => x.Id, existingEntry.Id),
                    Builders<TelegramDbEntry>.Filter.Eq(x => x.ChatId, existingEntry.ChatId)
                );

            return await _entryCollection.DeleteOneAsync(filter);
        }

        public async Task SaveEntry(TelegramDbEntry entry)
        {
            var filter = Builders<TelegramDbEntry>.Filter.Eq("chatId", entry.ChatId);
            await _entryCollection.ReplaceOneAsync(filter, entry, new ReplaceOptions { IsUpsert = true });
        }
    }
}