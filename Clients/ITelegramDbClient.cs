using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using NightscoutAlertBot.Models;

namespace NightscoutAlertBot.Clients
{
    public interface ITelegramDbClient
    {
        public Task<ICollection<TelegramDbEntry>> GetEntries();
        public Task<TelegramDbEntry> GetEntry(string chatId);
        public Task SaveEntry(TelegramDbEntry entry);
        public Task<DeleteResult> RemoveEntry(TelegramDbEntry entry);
    }
}