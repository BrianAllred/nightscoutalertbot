using System.Threading.Tasks;
using NightscoutAlertBot.Models;

namespace NightscoutAlertBot.Clients
{
    public interface INightscoutClient
    {
        public Task<NightscoutEntry> GetLastEntry();
    }
}