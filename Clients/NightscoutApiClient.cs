using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NightscoutAlertBot.Models;
using NightscoutAlertBot.Utilities;

namespace NightscoutAlertBot.Clients
{
    public class NightscoutApiClient : INightscoutClient
    {
        private readonly string _url;
        private readonly string _token;
        private readonly IHttpClientFactory _httpClientFactory;

        public NightscoutApiClient(EnvironmentConfig config, IHttpClientFactory httpClientFactory)
        {
            _url = $"{config.NightscoutApiUrl}/api/v1/";
            _token = config.NightscoutApiToken;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<NightscoutEntry> GetLastEntry()
        {
            using var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.GetAsync($"{_url}entries/?token={_token}&count=1");
            var body = await response.Content.ReadAsStringAsync();
            var entries = JsonConvert.DeserializeObject<List<NightscoutEntry>>(body);
            var entry = entries[0];
            return entry;
        }
    }
}