using System;
using MongoDB.Bson.Serialization.Attributes;

namespace NightscoutAlertBot.Models
{
    public class NightscoutEntry
    {
        [BsonElement("device")]
        public string Device { get; set; }
        [BsonElement("date")]
        public object Date { get; set; }
        [BsonElement("dateString")]
        public DateTime DateString { get; set; }
        [BsonElement("sgv")]
        public int Sgv { get; set; }
        [BsonElement("delta")]
        public double Delta { get; set; }
        [BsonElement("direction")]
        public string Direction { get; set; }
        [BsonElement("type")]
        public string Type { get; set; }
        [BsonElement("filtered")]
        public int Filtered { get; set; }
        [BsonElement("unfiltered")]
        public int Unfiltered { get; set; }
        [BsonElement("rssi")]
        public int Rssi { get; set; }
        [BsonElement("noise")]
        public int Noise { get; set; }
        [BsonElement("sysTime")]
        public DateTime SysTime { get; set; }
        [BsonElement("utcOffset")]
        public int UtcOffset { get; set; }
        [BsonElement("mills")]
        public object Mills { get; set; }

        public string GetTrend()
        {
            var trend = Direction switch
            {
                "Flat" => "stable",
                "FortyFiveUp" => "up",
                "FortyFiveDown" => "down",
                "SingleUp" => "up quickly",
                "SingleDown" => "down quickly",
                _ => string.Empty
            };
            return trend;
        }
    }
}