using MongoDB.Bson;

namespace NightscoutAlertBot.Models
{
    public class NightscoutDbEntry : NightscoutEntry
    {
        public ObjectId Id { get; set; }
    }
}