using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NightscoutAlertBot.Models
{
    public class TelegramDbEntry
    {
        public ObjectId Id { get; set; }
        [BsonElement("chatId")]
        public string ChatId { get; set; }
        [BsonElement("customLowMessage")]
        public string CustomLowMessage { get; set; }
        [BsonElement("customHighMessage")]
        public string CustomHighMessage { get; set; }

        public string GetLowAlertMessage(NightscoutEntry lastEntry)
        {
            var message = string.IsNullOrEmpty(CustomLowMessage) ? $"WARNING: Blood sugar low!" : CustomLowMessage;
            return message + $"\n\nBlood sugar is {lastEntry.Sgv} and trending {lastEntry.GetTrend()}.";
        }

        public string GetHighAlertMessage(NightscoutEntry lastEntry)
        {
            var message = string.IsNullOrEmpty(CustomHighMessage) ? $"WARNING: Blood sugar high!" : CustomHighMessage;
            return message + $"\n\nBlood sugar is {lastEntry.Sgv} and trending {lastEntry.GetTrend()}.";
        }
    }
}