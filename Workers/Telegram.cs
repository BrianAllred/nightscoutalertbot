using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NightscoutAlertBot.Clients;
using NightscoutAlertBot.Models;
using NightscoutAlertBot.Utilities;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types.Enums;

namespace NightscoutAlertBot.Workers
{
    public class Telegram : ITelegram
    {
        private readonly INightscoutClient _nightscoutClient;
        private readonly ITelegramDbClient _telegramDbClient;
        private readonly TelegramBotClient _telegramClient;
        private readonly EnvironmentConfig _config;
        private readonly Dictionary<string, TelegramDbEntry> _chatEntries = new();
        private bool _started;

        public Telegram(INightscoutClient nsClient, ITelegramDbClient telegramDbClient, EnvironmentConfig config)
        {
            _nightscoutClient = nsClient;
            _telegramDbClient = telegramDbClient;
            _telegramClient = new TelegramBotClient(config.TelegramBotToken);
            _config = config;
        }

        public async Task Start()
        {
            if (_started) return;
            _started = true;

            foreach (var entry in await _telegramDbClient.GetEntries())
            {
                _chatEntries.Add(entry.ChatId, entry);
            }

            // Fire and forget
            _ = Task.Run(() => CheckGlucoseLoop()).ConfigureAwait(false);

            var updateReceiver = new QueuedUpdateReceiver(_telegramClient);
            updateReceiver.StartReceiving();

            await foreach (var update in updateReceiver.YieldUpdatesAsync())
            {
                try
                {
                    if (update.Message is { } message)
                    {
                        var me = await _telegramClient.GetMeAsync();
                        var chatId = update.Message.Chat.Id;
                        var chatIdString = chatId.ToString();

                        if (update.Message.Type == MessageType.ChatMemberLeft && update.Message.LeftChatMember.Id == me.Id && _chatEntries.ContainsKey(chatIdString))
                        {
                            await _telegramDbClient.RemoveEntry(_chatEntries[chatIdString]);
                            _chatEntries.Remove(chatIdString);
                            continue;
                        }

                        if (update.Message.Type != MessageType.Text) continue;

                        var chatMember = await _telegramClient.GetChatMemberAsync(chatId, update.Message.From.Id);

                        if (message.Text.Equals("/start") && (update.Message.Chat.Type == ChatType.Private || chatMember.HasPermission()))
                        {
                            if (_chatEntries.ContainsKey(chatIdString)) continue;

                            await _telegramDbClient.SaveEntry(new TelegramDbEntry { ChatId = chatIdString });
                            _chatEntries.Add(chatIdString, await _telegramDbClient.GetEntry(chatIdString));
                            await _telegramClient.SendTextMessageAsync(chatId, "Registered this chat for blood sugar updates.", replyToMessageId: update.Message.MessageId);
                        }
                        else if (message.Text.Equals("/stop") && (update.Message.Chat.Type == ChatType.Private || chatMember.HasPermission()))
                        {
                            if (!_chatEntries.ContainsKey(chatIdString)) continue;

                            await _telegramDbClient.RemoveEntry(_chatEntries[chatIdString]);
                            _chatEntries.Remove(chatIdString);
                            await _telegramClient.SendTextMessageAsync(chatId, "Unregistered this chat from blood sugar updates.", replyToMessageId: update.Message.MessageId);
                        }
                        else if (message.Text.StartsWith("/setCustomLowMessage") && (update.Message.Chat.Type == ChatType.Private || chatMember.HasPermission()))
                        {
                            if (!_chatEntries.ContainsKey(chatIdString)) continue;

                            var customLowMessage = message.Text.Replace("/setCustomLowMessage", string.Empty).Trim();
                            _chatEntries[chatIdString].CustomLowMessage = customLowMessage;
                            await _telegramDbClient.SaveEntry(_chatEntries[chatIdString]);
                            await _telegramClient.SendTextMessageAsync(chatId, $"New custom low alert message set.", replyToMessageId: update.Message.MessageId);
                        }
                        else if (message.Text.StartsWith("/setCustomHighMessage") && (update.Message.Chat.Type == ChatType.Private || chatMember.HasPermission()))
                        {
                            if (!_chatEntries.ContainsKey(chatIdString)) continue;

                            var customHighMessage = message.Text.Replace("/setCustomHighMessage", string.Empty).Trim();
                            _chatEntries[chatIdString].CustomHighMessage = customHighMessage;
                            await _telegramDbClient.SaveEntry(_chatEntries[chatIdString]);
                            await _telegramClient.SendTextMessageAsync(chatId, $"New custom high alert message set.", replyToMessageId: update.Message.MessageId);
                        }
                        else if (message.Text.Equals("/current"))
                        {
                            if (!_chatEntries.ContainsKey(chatIdString)) continue;

                            var entry = await _nightscoutClient.GetLastEntry();
                            await _telegramClient.SendTextMessageAsync(chatId, $"Blood sugar is {entry.Sgv} and trending {entry.GetTrend()}.", replyToMessageId: update.Message.MessageId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private async Task CheckGlucoseLoop(CancellationToken cancellationToken = default)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    if (_chatEntries.Count > 0)
                    {
                        var lastEntry = await _nightscoutClient.GetLastEntry();
                        if (_config.LowAlertValue > 0 && lastEntry.Sgv <= _config.LowAlertValue)
                        {
                            foreach (var chatEntry in _chatEntries)
                            {
                                await _telegramClient.SendTextMessageAsync(chatEntry.Key, chatEntry.Value.GetLowAlertMessage(lastEntry), cancellationToken: cancellationToken);
                            }
                        }
                        else if (_config.HighAlertValue > 0 && lastEntry.Sgv >= _config.HighAlertValue)
                        {
                            foreach (var chatEntry in _chatEntries)
                            {
                                await _telegramClient.SendTextMessageAsync(chatEntry.Key, chatEntry.Value.GetHighAlertMessage(lastEntry), cancellationToken: cancellationToken);
                            }
                        }

                        await Task.Delay(_config.PollInterval * 1000, cancellationToken);
                    }
                    else
                    {
                        await Task.Delay(5000, cancellationToken);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}