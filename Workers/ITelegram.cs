using System.Threading.Tasks;

namespace NightscoutAlertBot.Workers
{
    public interface ITelegram
    {
        public Task Start();
    }
}