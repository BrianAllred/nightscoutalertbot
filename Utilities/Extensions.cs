using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace NightscoutAlertBot.Utilities
{
    public static class Extensions
    {
        public static bool HasPermission(this ChatMember chatMember)
        {
            return chatMember.Status is ChatMemberStatus.Creator or ChatMemberStatus.Administrator;
        }
    }
}