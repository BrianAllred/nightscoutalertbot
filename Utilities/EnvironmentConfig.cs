using static NightscoutAlertBot.Utilities.Enums;

namespace NightscoutAlertBot.Utilities
{
    public class EnvironmentConfig
    {
        public string TelegramBotToken { get; set; }
        public DataSource NightscoutDataSource { get; set; }
        public string NightscoutApiUrl { get; set; }
        public string NightscoutDbUrl { get; set; }
        public int NightscoutDbPort { get; set; }
        public string NightscoutApiToken { get; set; }
        public string NightscoutDbUsername { get; set; }
        public string NightscoutDbPassword { get; set; }
        public string NightscoutDbData { get; set; }
        public string NightscoutDbAuth { get; set; }
        public string TelegramDbUrl { get; set; }
        public int TelegramDbPort { get; set; }
        public string TelegramDbUsername { get; set; }
        public string TelegramDbPassword { get; set; }
        public string TelegramDbData { get; set; }
        public string TelegramDbAuth { get; set; }
        public int LowAlertValue { get; set; }
        public int HighAlertValue { get; set; }
        public int PollInterval { get; set; }
    }
}