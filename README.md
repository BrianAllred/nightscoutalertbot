# NightscoutAlertBot

[![pipeline status](https://gitlab.com/BrianAllred/nightscoutalertbot/badges/main/pipeline.svg)](https://gitlab.com/nightscoutalertbot/nightscoutalertbot/commits/main)

A Telegram bot used to notify users when Nightscout reports high and/or low blood sugar levels. Telegram chats to notify are stored in MongoDB, so deploy a Mongo instance alongside this bot or piggy-back on Nightscout's instance.

## Deployment

- Create a new Telegram bot by messaging [BotFather](http://t.me/BotFather) the message `/newbot`.
- Save the HTTP API token BotFather gives you!
- Deploy alert bot using Docker image at `registry.gitlab.com/brianallred/nightscoutalertbot`. Don't forget to set the required environment variables (detailed below)!
- OR download the artifact for your desired OS from the [latest version pipeline](https://gitlab.com/BrianAllred/nightscoutalertbot/-/pipelines) and make sure to set the required environment variables before you run it.

## Configuration

Configuration is managed with several environment variables.

- The `NS_API*` variables are required if `NS_DATASOURCE` is set to `API`. This is the recommended configuration.
- The `NS_DB*` variables are required if `NS_DATASOURCE` is set to `DB`.
- The `TG*` variables are always required.
- For ease of use, it's recommended to piggy-back on Nightscout's existing MongoDB instance for Telegram's data (with a different data database).

| **EnvVar**       | **Usage**                                                                       |
| ---------------- | ------------------------------------------------------------------------------- |
| NS_API_URL       | URL of the Nightscout server. (`https://nightscout.example.com`)                |
| NS_API_TOKEN     | Nightscout API token. Master token won't work, generate one in the admin panel. |
| NS_DATASOURCE    | Data source for Nightscout data. `API` is the default. (`API` or `DB`)          |
| NS_DB_URL        | Host for the Nightscout MongoDB database. (`localhost`)                         |
| NS_DB_PORT       | Port for the Nightscout MongoDB database. (`27017`)                             |
| NS_DB_USERNAME   | Username for the Nightscout MongoDB database. (`root`)                          |
| NS_DB_PASSWORD   | Password for the Nightscout MongoDB database. (`example`)                       |
| NS_DB_DATA       | Mongo database where the Nightscout entries live. (`test`)                      |
| NS_DB_AUTH       | Mongo database where the DB users live. (`admin`)                               |
| TG_DB_URL        | Host for the Telegram MongoDB database. (`localhost`)                           |
| TG_DB_PORT       | Port for the Telegram MongoDB database. (`27017`)                               |
| TG_DB_USERNAME   | Username for the Telegram MongoDB database. (`root`)                            |
| TG_DB_PASSWORD   | Password for the Telegram MongoDB database. (`example`)                         |
| TG_DB_DATA       | Mongo database where the Telegram entries live. (`tg`)                          |
| TG_DB_AUTH       | Mongo database where the DB users live. (`admin`)                               |
| TG_BOT_TOKEN     | Telegram bot token obtained from BotFather.                                     |
| LOW_ALERT_VALUE  | Value to alert on lows. 0 is disabled. (`0`)                                    |
| HIGH_ALERT_VALUE | Value to alert on highs. 0 is disabled. (`0`)                                   |
| POLL_INTERVAL    | Polling interval in seconds. (`300`)                                            |

## Usage

Once the bot is up and running, either message it directly using the link BotFather provided, or add it to a group chat.

| **Command** | **Result**                                                       | **Group Permission Required** |
| ----------- | ---------------------------------------------------------------- | ----------------------------- |
| `/start`    | Adds the current chat to the chat list that the bot will notify. | Creator / Administrator       |
| `/stop`     | Removes the current chat from the notification chat list.        | Creator / Administrator       |
| `/current`  | Display current blood sugar information on demand.               | None                          |
